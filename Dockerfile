FROM l.gcr.io/google/bazel:3.0.0
RUN apt-get update && apt-get install -y \
		cmake \
		&& rm -rf /var/lib/apt/lists/*
